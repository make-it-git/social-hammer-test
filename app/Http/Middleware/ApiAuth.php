<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $accessToken = $request->header('Access-Token');
        if (!$accessToken) {
            return response()->json([
                'ok' => false,
                'error' => 'Unathenticated'
            ], 400);
        }

        $user = User::where('access_token', $accessToken)->first();
        if (!$user) {
            return response()->json([
                'ok' => false,
                'error' => 'Unathenticated'
            ], 400);
        }
        
        Auth::loginUsingId($user->id);

        return $next($request);
    }
}
