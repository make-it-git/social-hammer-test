<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json([
            'ok' => true,
            'products' => Product::where('creator_id', $request->user()->id)
                ->filter($request->all())
                ->with('category')
                ->with('tagged')
                ->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    private function validateRequest(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'price' => 'required|integer|min:1',
            'description' => 'required|string',
            'image' => 'nullable|image|max:10000',
            'image_description' => 'required_with:image|string',
            'category_id' => 'nullable|exists:categories,id',
            'tags.*' => 'required|string|max:32',
        ]);
    }

    private function fillProduct(Request $request, Product $product)
    {
        $product->name = $request->get('name');
        $product->price = $request->get('price');
        $product->description = $request->get('description');
        $product->image_description = $request->get('image_description');
        $product->category()->associate($request->get('category_id'));
        $product->creator()->associate($request->user());
        if ($image = $request->file('image')) {
            $product->image = $image->store('products');
        }
    }

    private function fillTags(Request $request, Product $product)
    {
        $tags = $request->get('tags');
        if (is_array($tags)) {
            $product->retag($tags);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);
        $product = new Product;
        $this->fillProduct($request, $product);
        $product->save();
        $this->fillTags($request, $product);
        return response()->json(['ok' => true, 'product' => $product::with('tagged')->find($product->id)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $product = Product::where('creator_id', $request->user()->id)->where('id', $id)->first();
        if (!$product) abort(404);
        return response()->json([
            'ok' => true,
            'product' => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::where('creator_id', $request->user()->id)->where('id', $id)->first();
        if (!$product) abort(404);

        $this->validateRequest($request);
        $this->fillProduct($request, $product);
        $product->save();
        $this->fillTags($request, $product);

        return response()->json([
            'ok' => true,
            'product' => $product::with('tagged')->find($product->id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $product = Product::where('creator_id', $request->user()->id)->where('id', $id)->first();
        if (!$product) abort(404);

        $product->delete();
        return response()->json([
            'ok' => true
        ]);
    }
}
