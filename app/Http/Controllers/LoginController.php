<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'login' => 'required|string',
            'password' => 'required|string'
        ]);

        $login = $request->get('login');
        $password = $request->get('password');

        $user = User::where('login', $login)->first();
        if (!$user) {
            $user = User::where('email', $login)->first();
        }
        if (!$user) {
            return response()->json([
                'ok' => false,
                'error' => 'User not found'
            ]);
        }
        
        if (Hash::check($password, $user->password)) {
            $user->access_token = str_random(128);
            $user->save();
            return response()->json([
                'ok' => true,
                'access_token' => $user->access_token
            ]);
        }

        return response()->json([
            'ok' => false,
            'error' => 'Invalid password'
        ]);
    }
}
