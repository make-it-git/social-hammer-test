<?php namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class ProductFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function name($name)
    {
        return $this->where(function($q) use ($name)
        {
            return $q->where('name', 'LIKE', "%$name%");
        });
    }

    public function description($description)
    {
        return $this->where(function($q) use ($description)
        {
            return $q->where('description', 'LIKE', "%$description%");
        });
    }

    public function priceFrom($price)
    {
        return $this->where(function($q) use ($price)
        {
            return $q->where('price', '>=', $price);
        });
    }

    public function priceTo($price)
    {
        return $this->where(function($q) use ($price)
        {
            return $q->where('price', '<=', $price);
        });
    }

    public function tags($tags)
    {
        return $this->withAllTags($tags);
    }
}
