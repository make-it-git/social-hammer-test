<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use EloquentFilter\Filterable;
use Conner\Tagging\Taggable;

class Product extends Model
{
    use Filterable;
    use Taggable;

    protected $appends = ['short_description'];

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function getShortDescriptionAttribute()
    {
        return Str::words($this->description, 8);
    }
}
