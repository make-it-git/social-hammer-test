<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\Product;
use App\Category;
use Illuminate\Http\UploadedFile;
use Storage;

class ProductTest extends TestCase
{
    use DatabaseTransactions;

    private function createUser($accessToken)
    {
        $user = new User;
        $user->login = 'login';
        $user->email = uniqid('email') . '@example.com';
        $user->password = '123456';
        $user->access_token = $accessToken;
        $user->save();
        return $user;
    }

    private function createProduct(User $user)
    {
        $product = new Product;
        $product->name = 'product';
        $product->price = 1;
        $product->description = 'description';
        $product->creator()->associate($user);
        $product->save();
        return $product;
    }

    public function testCreation()
    {
        $token = '95679868346834863';
        $user = $this->createUser($token);

        $response = $this->post('/api/products')->json();
        $this->assertEquals($response['error'], 'Unathenticated');

        $response = $this->post('/api/products', [], ['Access-Token' => $token])->json();
        $this->assertEquals($response['errors']['name'], 'The name field is required.');
        $this->assertEquals($response['errors']['price'], 'The price field is required.');
        $this->assertEquals($response['errors']['description'], 'The description field is required.');

        Storage::fake('products');

        $response = $this->post('/api/products', [
            'name' => 'Product 1',
            'price' => '123',
            'description' => 'Text',
            'image' => UploadedFile::fake()->image('picture.jpg'),
        ], ['Access-Token' => $token])->json();
        $this->assertEquals($response['errors']['image_description'], 'The image description field is required when image is present.');

        $response = $this->post('/api/products', [
            'name' => 'Product 1',
            'price' => '123',
            'description' => 'Text',
            'image' => UploadedFile::fake()->image('picture.jpg'),
            'image_description' => 'image description'
        ], ['Access-Token' => $token])->json();
        $this->assertEquals(true, $response['ok']);
        $this->assertEquals($user->id, $response['product']['creator_id']);

        $savedImage = $response['product']['image'];
        Storage::assertExists($savedImage);
    }

    public function testCreationWithTags()
    {
        $token = '95679868346834863';
        $user = $this->createUser($token);

        $response = $this->post('/api/products', [
            'name' => 'Product 1',
            'price' => '123',
            'description' => 'Text',
            'tags' => ['some tag 1', 'some tag 2', '']
        ], ['Access-Token' => $token])->json();
        $this->assertEquals($response['errors']['tags.2'], 'The tags.2 field is required.');

        $response = $this->post('/api/products', [
            'name' => 'Product 1',
            'price' => '123',
            'description' => 'Text',
            'tags' => ['some tag 1', 'some tag 2']
        ], ['Access-Token' => $token])->json();
        $this->assertEquals('Some Tag 1', $response['product']['tagged'][0]['tag_name']);
        $this->assertEquals('Some Tag 2', $response['product']['tagged'][1]['tag_name']);
    }

    public function testShow()
    {
        $token1 = '95679868346834863';
        $token2 = '3209734067934';

        $user1 = $this->createUser($token1);
        $user2 = $this->createUser($token2);

        $product1 = $this->createProduct($user1);
        $product2 = $this->createProduct($user2);

        $response = $this->get('/api/products/' . $product1->id, ['Access-Token' => $token1])->json();
        $this->assertEquals($product1->id, $response['product']['id']);

        $response = $this->get('/api/products/' . $product1->id, ['Access-Token' => $token2])->json();
        $this->assertEquals($response['error'], 'Not found');
    }

    public function testUpdate()
    {
        $token1 = '95679868346834863';
        $token2 = '3209734067934';

        $user1 = $this->createUser($token1);
        $user2 = $this->createUser($token2);

        $product1 = $this->createProduct($user1);
        $product2 = $this->createProduct($user2);

        $response = $this->put('/api/products/' . $product1->id, [
            'name' => 'other name',
            'price' => 3333,
            'description' => 'other description',
            'tags' => ['tag test', 'tag other test']
        ], ['Access-Token' => $token1])->json();
        $this->assertEquals($product1->id, $response['product']['id']);
        $this->assertEquals('other name', $response['product']['name']);
        $this->assertEquals(3333, $response['product']['price']);
        $this->assertEquals('other description', $response['product']['description']);
        $this->assertEquals('Tag Test', $response['product']['tagged'][0]['tag_name']);
        $this->assertEquals('Tag Other Test', $response['product']['tagged'][1]['tag_name']);

        $response = $this->put('/api/products/' . $product1->id, [
            'name' => 'other name',
            'price' => 3333,
            'description' => 'other description'
        ], ['Access-Token' => $token2])->json();
        $this->assertEquals($response['error'], 'Not found');
    }

    public function testDelete()
    {
        $token1 = '95679868346834863';
        $token2 = '3209734067934';

        $user1 = $this->createUser($token1);
        $user2 = $this->createUser($token2);

        $product1 = $this->createProduct($user1);
        $product2 = $this->createProduct($user2);

        $response = $this->delete('/api/products/' . $product1->id, [], ['Access-Token' => $token2])->json();
        $this->assertEquals($response['error'], 'Not found');

        $this->assertNotNull(Product::find($product1->id));

        $response = $this->delete('/api/products/' . $product1->id, [], ['Access-Token' => $token1])->json();
        $this->assertEquals(true, $response['ok']);

        $this->assertNull(Product::find($product1->id));
    }

    public function testCreationWithCategory()
    {
        $token = '95679868346834863';
        $user = $this->createUser($token);

        $category = new Category;
        $category->name = 'Root category';
        $category->save();

        $child = new Category;
        $child->name = 'Child category';
        $category->appendNode($child);

        $response = $this->post('/api/products', [
            'name' => 'Product 1',
            'price' => '123',
            'description' => 'Text',
            'category_id' => 0,
        ], ['Access-Token' => $token])->json();
        $this->assertEquals($response['errors']['category_id'], 'The selected category id is invalid.');

        $response = $this->post('/api/products', [
            'name' => 'Product 1',
            'price' => '123',
            'description' => 'Text',
            'category_id' => $category->id,
        ], ['Access-Token' => $token])->json();
        $this->assertEquals($response['product']['category_id'], $category->id);

        $response = $this->post('/api/products', [
            'name' => 'Product 1',
            'price' => '123',
            'description' => 'Text',
            'category_id' => $child->id,
        ], ['Access-Token' => $token])->json();
        $this->assertEquals($response['product']['category_id'], $child->id);
    }

    public function testPagination()
    {
        $token = '95679868346834863';
        $user = $this->createUser($token);

        $category = new Category;
        $category->name = 'Root category';
        $category->save();

        $child = new Category;
        $child->name = 'Child category';
        $category->appendNode($child);

        for($i = 1; $i <= 25; $i++) {
            $product = new Product;
            $product->name = 'Product ' . $i;
            $product->price = 1;
            $product->description = '';
            $product->category()->associate($i % 2 == 0 ? $category : $child);
            $product->creator()->associate($user);
            $product->save();
        }

        $response = $this->get('/api/products', ['Access-Token' => $token])->json();
        $this->assertEquals($response['products']['data'][0]['name'], 'Product 1');
        $this->assertEquals($response['products']['data'][0]['category']['name'], 'Child category');
        $this->assertEquals($response['products']['data'][1]['name'], 'Product 2');
        $this->assertEquals($response['products']['data'][1]['category']['name'], 'Root category');

        $response = $this->get('/api/products?page=2', ['Access-Token' => $token])->json();
        $this->assertEquals($response['products']['data'][0]['name'], 'Product 11');
        $this->assertEquals($response['products']['data'][0]['category']['name'], 'Child category');
        $this->assertEquals($response['products']['data'][1]['name'], 'Product 12');
        $this->assertEquals($response['products']['data'][1]['category']['name'], 'Root category');
    }

    public function testFilter()
    {
        $token = '95679868346834863';
        $user = $this->createUser($token);

        $category1 = new Category;
        $category1->name = 'Root category 1';
        $category1->save();

        $category2 = new Category;
        $category2->name = 'Root category 2';
        $category2->save();

        for($i = 1; $i <= 25; $i++) {
            $product = new Product;
            $product->name = 'Product ' . $i;
            $product->price = $i * 5;
            $product->description = 'Description ' . $i . ' and some more words in description appended to make it long';
            $product->category()->associate($i % 2 == 0 ? $category1 : $category2);
            $product->creator()->associate($user);
            $product->save();
        }

        $response = $this->get('/api/products?name=Product 22', ['Access-Token' => $token])->json();
        $this->assertCount(1, $response['products']['data']);

        $response = $this->get('/api/products?name=Product 2', ['Access-Token' => $token])->json();
        $this->assertCount(7, $response['products']['data']);

        $response = $this->get('/api/products?name=Product 2&description=Description 23', ['Access-Token' => $token])->json();
        $this->assertCount(1, $response['products']['data']);
        $this->assertEquals('Description 23 and some more words in description...', $response['products']['data'][0]['short_description']);
        $this->assertEquals('Description 23 and some more words in description appended to make it long', $response['products']['data'][0]['description']);

        $response = $this->get('/api/products?price_from=10&price_to=20', ['Access-Token' => $token])->json();
        $this->assertCount(3, $response['products']['data']);
        $this->assertEquals(10, $response['products']['data'][0]['price']);
        $this->assertEquals(15, $response['products']['data'][1]['price']);
        $this->assertEquals(20, $response['products']['data'][2]['price']);
    }

    public function testFilterByTags()
    {
        $token = '95679868346834863';
        $user = $this->createUser($token);

        $tag1 = 'first tag';
        $tag2 = 'second tag';
        $tag3 = 'third tag';

        $createProduct = function($tags) use($user) {
            $product = new Product;
            $product->name = '';
            $product->price = 1;
            $product->description = '';
            $product->creator()->associate($user);
            $product->save();
            $product->retag($tags);
            return $product;
        };

        $productTag1 = $createProduct([$tag1]);
        $productTag1And2 = $createProduct([$tag1, $tag2]);
        $productTag1And2And3 = $createProduct([$tag1, $tag2, $tag3]);
        $productTag2And3 = $createProduct([$tag2, $tag3]);

        $response = $this->get('/api/products?tags=' . $tag1, ['Access-Token' => $token])->json();
        $this->assertCount(3, $response['products']['data']);

        $response = $this->get('/api/products?tags=' . $tag2, ['Access-Token' => $token])->json();
        $this->assertCount(3, $response['products']['data']);

        $response = $this->get('/api/products?tags=' . $tag3, ['Access-Token' => $token])->json();
        $this->assertCount(2, $response['products']['data']);

        $response = $this->get('/api/products?tags=' . $tag3 . ',' . $tag2, ['Access-Token' => $token])->json();
        $this->assertCount(2, $response['products']['data']);

        $response = $this->get('/api/products?tags=' . $tag1 . ',' . $tag2 . ',' . $tag3, ['Access-Token' => $token])->json();
        $this->assertCount(1, $response['products']['data']);
    }
}
