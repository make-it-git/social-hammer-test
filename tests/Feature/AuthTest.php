<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use DB;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    private function createUser($login, $email, $password)
    {
        $user = new User;
        $user->login = $login;
        $user->email = $email;
        $user->password = $password;
        $user->save();
        return $user;
    }

    public function testAuth()
    {
        $user = $this->createUser('login', 'email@example.com', '123456');

        $response = $this->post('/api/login')->json();
        $this->assertEquals($response['errors']['login'], 'The login field is required.');
        $this->assertEquals($response['errors']['password'], 'The password field is required.');

        $response = $this->post('/api/login', [
            'login' => 'invalid',
            'password' => '123456'
        ])->json();
        $this->assertEquals($response['error'], 'User not found');

        $response = $this->post('/api/login', [
            'login' => 'login',
            'password' => '1234567'
        ])->json();
        $this->assertEquals($response['error'], 'Invalid password');

        $response = $this->post('/api/login', [
            'login' => 'email@example.com',
            'password' => '1234567'
        ])->json();
        $this->assertEquals($response['error'], 'Invalid password');

        $response = $this->post('/api/login', [
            'login' => 'email@example.com',
            'password' => '123456'
        ])->json();

        $accessToken = $user->fresh()->access_token;
        $this->assertEquals($response['access_token'], $accessToken);
    }
}
